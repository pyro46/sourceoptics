
![](https://bitbucket.org/laserllama/sourceoptics/raw/3d9517a1e803e181e7d85338f41b95b0cae9d20e/source_optics/static/logo_bg.png)

![](https://img.shields.io/badge/dynotherms-connected-blue) 
![](https://img.shields.io/badge/infracells-up-green) 
![](https://img.shields.io/badge/megathrusters-go-green)

Source Optics
=============

Source Optics is an advanced source code repository dashboard, focused on 
understanding evolving code activity in an organization and the teams that 
work on them.

Read all about use cases at  http://sourceoptics.io/

Installation, Setup, and Usage
==============================

Source Optics is a stand-alone web applicaton.  It is built on Python3 and 
Django, using PostgreSQL for a database, making it exceptionally easy to 
deploy. You should be up and running in about 30 minutes, and setup is fairly 
standard as Django applications go.

See [INSTALL.md](https://github.com/sourceoptics/source_optics/blob/master/INSTALL.m
d) for detailed instructions.

License
=======

All source is provided under the Apache 2 license, (C) 2018-2019, All Project 
Contributors.

Newsletter
==========

A weekly newsletter is available for signup at the bottom of 
http://sourceoptics.io/ - it's a great way to keep up with new features in 
development, ideas, to participate in surveys, and more. 

Code Contribution Preferences
=============================

All contributors need to subscribe to the weekly newsletter to keep up with major
developments.

Features should be discussed (email michael@michaeldehaan.net) before sending 
code to avoid extra development efforts.

Please not send any submissions to tweak PEP8, pylint, or other code 
preferences, or to add integrations with any favored CI/CD tool, editor,
or deployment system.

BitBucket pull requests are accepted, but a bitbucket account is not required to
contribute. You can submit code changes via git-format-patch by email to 
michael@michaeldehaan.net.

Thank you!
