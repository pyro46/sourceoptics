# Copyright 2018 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# dataframes.py - code behind getting meaningful pandas dataframes (largely for graphing) from Statistic
# and FileChange model querysets, feeding graphs.py

import django.utils.timezone as timezone
from ..models import Statistic, Author, FileChange
import pandas
from django.conf import settings
from dateutil import relativedelta
import dateutil.rrule as rrule
from django.db.models import Sum, Count

# fields valid for axes or tooltips in time series graphs.  These are added to each element in the pandas
# dataframes for Statistic objects - FileChange stats (path segment graphing support) have fixed data.

# FIXME: this should be moved to the Statistics class.
TIME_SERIES_FIELDS = [
    'day',
    'date',
    'lines_changed',
    'commit_total',
    'author_total',
    'average_commit_size',
    'files_changed',
    'days_since_seen',
    'days_before_joined',
    'longevity',
    'days_active',
    'creates',
    'edits',
    'moves',
    'repo'
]

TZ = timezone.get_current_timezone()

def get_basic_dataframe(scope):
    """
    For a given page - which could be per user or per repo, figure out what is wanted
    based on the scope parameters and return a Pandas dataframe that can be used to display
    graphs.
    """
    return team_time_series(scope)

def get_advanced_dataframe(scope, aspect='commit_total'):
    """
    Similar to get_basic_dataframe, this version generates top N author (or repo) information,
    and is capable of producing stacked area or line graphs.
    """
    (df, top) = (None, None)
    if scope.multiple_repos_selected():
        # comparing repositories since "repos=" was on the querystring
        (df, top) = team_time_series(scope)
    else:
        # comparing authors instead
        (df, top) = top_author_time_series(scope, aspect=aspect)
    return (df, top)

def get_interval(scope, start, end):
    """
    Attempts to decide on a good granularity for a graph (week, month, etc) when it is not provided.
    FIXME: This should be moved into _compute_interval in scope.py
    """
    if scope.interval:
        return scope.interval
    print("INTV=%s" % scope.interval)
    delta = end-start
    if delta.days > 365:
        return 'WK'
    else:
        return 'DY'

def top_authors(scope, aspect='commit_total', limit=9):
    """
    Return the top N authors for a repo based on a specified aspect. We default to the top 9 based
    on the default color palette in altair currently - it could be larger if we supply a better custom palette.
    """

    filter_set = Statistic.objects.filter(
        interval=scope.interval,
        author__isnull=False,
        repo=scope.repo,
        start_date__range=(scope.start, scope.end)
    ).values('author_id').annotate(total=Sum(aspect)).order_by('-total')[0:limit]

    return [ x for x in Author.objects.filter(pk__in= [ t['author_id'] for t in filter_set ]).all() ]

def top_authors_for_path(scope, limit=5):

    """
    Used by the file explorer, this variant of top_authors works on FileChange records not statistics records,
    and needs to work a little differently. We could select more authors but are defaulting to less to avoid
    page clutter.  This really could be configurable in settings.
    """

    filter_set = FileChange.objects.filter(
        file__path=scope.path,
        commit__repo=scope.repo,
        commit__commit_date__range=(scope.start, scope.end)
    )
    if scope.file:
        filter_set = filter_set.filter(file__name=scope.file)

    filter_set = filter_set.values('commit__author_id').annotate(total=Count('commit'))[0:limit]
    return [ x for x in Author.objects.filter(pk__in= [ t['commit__author_id'] for t in filter_set ]).all() ]

def _interval_queryset_not_by_author(scope):

    # not "by_author" here means not partitioned by author, we still allow narrowing the stats down
    # to those by a specific author.

    totals = None

    if scope.interval != 'LF':
        # time series data for a specific repo, not segmented by author
        totals = Statistic.objects.select_related('repo', ).filter(
            interval=scope.interval,
            start_date__range=(scope.start, scope.end)
        )
    else:
        # this would only be good for scatter plotting the lifetime statistics of a repo against
        # each other.
        if scope.start is None or scope.end is None:
            totals = Statistic.objects.select_related('repo').filter(interval=scope.interval)
        else:
            totals = Statistic.objects.select_related('repo').filter(interval=scope.interval)

    if scope.repo:
        totals = totals.filter(repo=scope.repo)
    if not scope.author:
        totals = totals.filter(author__isnull=True)
    else:
        totals = totals.filter(author=scope.author)
    return totals




def _interval_queryset_by_author(scope):
    if scope.interval != 'LF':
        # we're graphing time series data
        totals = Statistic.objects.select_related('repo', 'author').filter(
            interval=scope.interval,
            author__isnull=False,
            start_date__range=(scope.start, scope.end)
        )

    else:
        # not time series data
        # probably going to be used in a scatter plot comparing authors

        totals = Statistic.objects.select_related('repo', 'author').filter(
            interval=scope.interval,
            author__isnull=False
        )
        if scope.start and scope.end:
            # while we requested a lifetime scatter plot, we can still trim the lifetime stats by excluding authors
            # who were not active within that time range
            totals = totals.filter(author__commits__commit_date__range=(scope.start, scope.end))

    if scope.repo:
        totals = totals.filter(repo=scope.repo)

    if scope.author:
        # restricting a graph to a single author
        totals = totals.filter(author=scope.author)

    return totals

def _interval_queryset(scope, by_author=False, aspect=None, limit_top_authors=False):


    """
    Returns a queryset of statistics that can be used to generate a dataframe for a plot.

    behavior modifiers:

    by_author - if true, returns datapoints for each author seperately rather than the repo total across all authors.
    limit_top_authors, if true, further modifies by_author to return a queryset for the top authors differently from a queryset for the rest.
    """

    limited_to = None
    inverse = None
    totals = None

    assert scope.interval != 'LF'

    if not by_author:
        totals = _interval_queryset_not_by_author(scope)

    else:
        # we are segmenting by author...
        if limit_top_authors:
            # if we are carving out top author data, find out who they are
            limited_to = top_authors(scope, aspect=aspect)
        totals = _interval_queryset_by_author(scope)

        if limit_top_authors:
            # if we are limiting top authors, carve out the statistics into the two buckets at this time.
            # the 'inverse' bucket contains the 'non-top' authors, which will be later aggregrated back together to
            # form composite stats.
            filtered = totals.filter(author__in=limited_to).select_related('author')
            inverse = totals.exclude(author__in=limited_to).select_related('author')
            return (filtered, limited_to, inverse)

    # we weren't segmenting by top authors, so we only have one queryset to return...
    return (totals.order_by('author','start_date').select_related('author'), None, None)


def _simplify_fields(item, fields, data, repo, for_aggregate=False):
    """
    Make the fields in a dataframe into something altair can consume.
    This has slightly different handling if 'for_aggregate' is true, which is used to process the OTHER
    collection of authors not in the top authors list.
    """
    for f in fields:
        if f == 'repo':
            # the queryset was annotated with the repo object, but we need the name
            data[f].append(repo.name)
        elif f in ['date', 'day']:
            # altair doesn't like datetimes and needs strings
            if not for_aggregate:
                data[f].append(str(item.start_date))
            else:
                data[f].append(str(item['start_date']))
        elif f == 'author':
            # for author records, use the full name if available
            # if not, pick the email address
            if not for_aggregate:
                data[f].append(item.author.get_display_name())
            else:
                data[f].append('OTHER')

        else:
            # all other fields can be transferred verbatim
            if not for_aggregate:
                data[f].append(getattr(item, f, -1))
            else:
                f2 = "annotated_%s" % f
                if f2 in item:
                    data[f].append(item[f2])
                else:
                    data[f].append(-1)


def _interval_queryset_to_dataframe(repo, totals=None, fields=None, inverse=None):
    """
    Takes a queryset from the 'interval_queryset' function and converts it into a
    usable Pandas dataframe for graphing.

    :param repo: repository object
    :param totals: a statistics queryset
    :param fields: fields to include in the dataframe
    :param start: beginning of the date range
    :param end: end of the date range
    :param interval: DY, WK, MN
    :param limited_to: a set of authors that are in the primary dataset
    :param inverse: if provided, an aggregrate of authors not in the primary dataset
    :return: (datastructure for dataframe, list of fields used)
    """

    data = dict()

    for f in fields:
        data[f] = []

    # load the dataframe with the queryset results we have

    for t in totals:
        _simplify_fields(t, fields, data, repo)

    if inverse:
        # the "inverse" queryset exists to select users *NOT* in the top authors list
        # and isn't shown in all graph types
        inverse = Statistic.annotate(inverse.values('start_date'))
        for x in inverse.all():
            _simplify_fields(x, fields, data, repo, for_aggregate=True)

    return (data, fields)


def _get_graph_interval(scope, repo):
    """
    The graph will normally get the interval from the query string - except in some cases it can be too
    wide. When the interval is too wide, the graph will essentially choke performance wise and also will not
    render as smoothly as it needs to, because there are too many data points when it is irregular. This
    function will use the suggested interval unless we deem it "too wide" of a time span, and then will
    attempt to use a wider interval.
    """

    interval = scope.interval

    start = scope.start
    earliest = None
    if repo:
        earliest = repo.earliest_commit_date()
        if (earliest and start < earliest):
            start = earliest

    if not interval:
        interval = get_interval(scope, start, scope.end)

    delta = (scope.end - start).days

    # FIXME: the scope parameter to this function is basically ignored. it probably can be removed.

    # this code needs to use the earlist commit date of the repo, not the actual date

    if interval == 'WK' and (delta > settings.GRAPH_MAX_DAYS_RESOLUTION):
        # data points get very un-smooth with too much on the graph
       # user can always dial in the time range to get something more granular
        interval = 'MN'

    return interval

def _get_dataframe_fields(by_author):
    fields = TIME_SERIES_FIELDS[:]
    if by_author:
        fields.append('author')
    return fields

def _get_dataframe(scope, by_author=False, interval=None, limit_top_authors=False, aspect=None):

    """
    this basically is a combo to call _interval_queryset and then _queryset_to_dataframe correctly
    based on context.

    REFACTOR: this should probably just be split into two functions or more so the names would
    better indicate the variations. In doing so (FIXME), we can just move the basic code inot the *_time_series
    functions below.
    """


    scope.interval = _get_graph_interval(scope, scope.repo)
    fields = _get_dataframe_fields(by_author)

    # holds the data that will be used to create the dataframe

    df_data = dict()
    limited_to_authors = None

    if scope.multiple_repos_selected():

        # getting dataframes for multiple repos - this form does NOT (yet?) support filtering by a single author
        # and currently works by executing one set of queries per repo which could get expensive if too many are selected.
        # (not maintaining the code complexity might still be worth keeping it this way).

        for repo in scope.repos:

            scope.repo = repo
            (totals, limited_to_authors, inverse) = _interval_queryset(scope, by_author=by_author, aspect=aspect, limit_top_authors=limit_top_authors)

            # merge the dataframes together
            (pre_df, fields) = _interval_queryset_to_dataframe(repo, totals=totals, fields=fields, inverse=inverse)
            for f in fields:
                if not f in df_data:
                    df_data[f] = []
                df_data[f].extend(pre_df[f])

    else:
        # getting data for a single repo, which may be partitioned by author.

        (totals, limited_to_authors, inverse) = _interval_queryset(scope, by_author=by_author, aspect=aspect, limit_top_authors=limit_top_authors)
        (df_data, fields) = _interval_queryset_to_dataframe(scope.repo, totals=totals, fields=fields, inverse=inverse)

    df = pandas.DataFrame(df_data, columns=fields)
    return (df, limited_to_authors)

def team_time_series(scope):
    """
    FIXME: rename (unclear)
    a team_time_series is a bit of a misnomer - really this is a per-repo dataframe that does not split the repo data by author
    and shows overall stats.  It CAN work on multiple repos.
    """
    (df, _) = _get_dataframe(scope, by_author=False)
    return (df, None)

def author_time_series(scope):
    """
    The author_time_series is not yet written, this was intended to do a cross-repo dataframe for a single author.
    FIXME: implement
    FIXME: rename (unclear)
    """
    (df, _) = _get_dataframe(scope, by_author=True)
    return (df, None)

def top_author_time_series(scope, aspect=None):
    # FIXME: rename (unclear)
    """
    Return a segmented dataframe showing statistics by author.
    """
    (df, top) = _get_dataframe(scope, by_author=True, aspect=aspect, limit_top_authors=True)
    return (df, top)

def path_segment_series(scope, top_authors):
    """
    The graphs for path segments in the file browser work differently than the repository graphs
    because they can't operate on Statistic objects and must use FileChange objects.  As such,
    much of the code here is a bit more manual and duplicated and (FIXME) needs to be moved
    back into the FileChange classes as this is refactored.

    This function generates a dataframe for a subdirectory or file, as opposed to a repo.
    A lot less data is available at this granularity because it would be excessively expensive
    to track all of this more deeply.

    FIXME: refactor lots.
    """
    # a series dealing with a specific directory of file changes

    assert scope.repo is not None

    fields = [ 'commits', 'date', 'author' ]
    path = scope.path
    if path == '/':
        path = ''

    repo = scope.repo

    earliest = repo.earliest_commit_date()
    latest = repo.latest_commit_date()

    if scope.start < earliest:
        scope.start = earliest

    if scope.end > latest:
        scope.end = latest

    # NOTE: these are NOT rolled up by date, so this could get a bit large.
    results = []

    scope.interval = 'WK'
    if (scope.end - scope.start).days > 700:
        scope.interval = 'MN'

    rule_pattern = None
    next_dt = None

    if scope.interval == 'WK':
        rule_pattern = rrule.rrule(rrule.WEEKLY, dtstart=scope.start, until=scope.end)
    else:
        rule_pattern = rrule.rrule(rrule.MONTHLY, dtstart=scope.start, until=scope.end)

    all_authors = top_authors[:]
    all_authors.append('OTHER')

    for dt in rule_pattern:
        if scope.interval == 'WK':
            next_dt = dt + relativedelta.relativedelta(weeks=1)
        else:
            next_dt = dt + relativedelta.relativedelta(months=1)

        for author in all_authors:

            count = None
            if scope.file is None:
                count = FileChange.objects.select_related('commit','file').filter(
                    commit__repo=scope.repo,
                    file__path__startswith=path,
                    commit__commit_date__range=(dt, next_dt)
                )
            else:
                count = FileChange.objects.select_related('commit','file').filter(
                    commit__repo=scope.repo,
                    file__path=path,
                    file__name=scope.file,
                    commit__commit_date__range=(dt, next_dt)
                )

            top_ids = [x.pk for x in top_authors]

            if author != 'OTHER':
                count = count.filter(commit__author=author)
            else:
                count = count.exclude(commit__author__pk__in=top_ids)
            count = count.count()

            k = author
            if k != 'OTHER':
                k = k.email

            item = dict(
                date = str(dt),
                commits = count,
                author = k,
            )

            results.append(item)

    return pandas.DataFrame(results, columns=fields)











